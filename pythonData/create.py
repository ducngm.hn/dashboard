import pandas as pd
import os
import os.path as path
import script.db1 as db1
import script.db2 as db2
import script.db3 as db3
import sys


pathDataRaw = path.abspath(path.join(os.getcwd(), "dataRaw/"))
pathDataCreated = path.abspath(
    path.join(os.getcwd(), "dataCreated/"))

# test = path.abspath(path.join(os.getcwd(),"pythonData/dataRaw/test3.xlsx"))
# xls = pd.ExcelFile(test)
# dfx = pd.read_excel(xls, 'LoanDist')

# db3.Chart1Limit(dfx,'SMap Jul')


def run():
    
    fileData = path.abspath(path.join(pathDataRaw, "masterData.xlsx"))
    xls = pd.ExcelFile(fileData)
    dfx = pd.read_excel(xls, 'data')
    for i in range(0, len(dfx)):
        createData(dfx.loc[i, "DataFile"], dfx.loc[i, "Time"])

    # print(dfx)


def createData(fileName, time):
    print("==> Start create ", fileName, time)
    fileData = path.abspath(path.join(pathDataRaw, fileName))
    xls = pd.ExcelFile(fileData)
    # db1
    dfx = pd.read_excel(xls, 'db1_financialsummary')
    db1.Financial_Summary(dfx, time)

    dfx = pd.read_excel(xls, 'db1_loangroup')
    db1.Loan_Group(dfx, time)

    dfx = pd.read_excel(xls, 'db1_crdistribution')
    db1.CR_Distribution(dfx, time)

    dfx = pd.read_excel(xls, 'db1_defaultpattern')
    db1.Default_Pattern(dfx, time)

    dfx = pd.read_excel(xls, 'db1_defaultanalysistable')
    db1.Default_Analysis_Table(dfx, time)

    dfx = pd.read_excel(xls, 'db1_rejectionanalysis')
    db1.Rejection_Analysis(dfx, time)

    # db2
    dfx = pd.read_excel(xls, 'db2_gini')
    db2.Gini(dfx, time)

    dfx = pd.read_excel(xls, 'db2_smap')
    db2.SMap(dfx, time)
    # db3
    # db3_currentstrategy
    # db3_extendterm
    # db3_reduceamount
    # db3_limitshading
    # db3_smap
    dfx = pd.read_excel(xls, 'db3_currentstrategy')
    db3.Chart1(dfx, "currentstrategy", time)

    dfx = pd.read_excel(xls, 'db3_extendterm')
    db3.Chart1(dfx, "extendterm", time)

    dfx = pd.read_excel(xls, 'db3_reduceloanamount')
    db3.Chart1(dfx, "reduceloanamount", time)

    dfx = pd.read_excel(xls, 'db3_limitshading')
    db3.Chart1Limit(dfx, time)

    dfx = pd.read_excel(xls, 'db3_smap')
    db3.SMap(dfx, time)

    print("==> Done data ", fileName, time)


run()
