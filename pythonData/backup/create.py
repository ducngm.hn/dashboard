import pandas as pd

xls = pd.ExcelFile('data.xlsx')
df = pd.read_excel(xls, 'Sheet1')
# df2 = pd.read_excel(xls, 'Sheet2')

# xls = pd.read_excel("test.xlsx")

# sheetX = xls.parse(0) #2 is the sheet number

# # df2.to_json(path_or_buf='output_path2.json', orient='table')
# df1.to_json(path_or_buf='output_path1.json', orient='table')

# f1 = open('file1.txt', 'r')
# f2 = open('file2.txt', 'w')
# for line in f1:
#     f2.write(line.replace('old_text', 'new_text'))
# f1.close()
# f2.close()

# df_sample.sort_values("point1",ascending=False)  #point1 giam dan ,"Bad Loan"


class LoadData(object):
    n = 0
    d = 0
    step = 0
    PerfectModel = 0
    ThisModel = 0
    AccuracyRatio = 0
    dfData = None

    # The class "constructor" - It's actually an initializer 
    def __init__(self, n, d, step,PerfectModel,ThisModel,AccuracyRatio,dfData):
        self.n = n
        self.d = d
        self.step = step
        self.PerfectModel = PerfectModel
        self.ThisModel = ThisModel
        self.AccuracyRatio = AccuracyRatio
        self.dfData = dfData

def makeData(tmpDf):
    tmpDf = tmpDf.reset_index(drop=True)
    tmpDf['Index'] = tmpDf.index
    tmpDf = tmpDf.sort_values(["Expected Loss", "Index"], ascending=[False, True])
    tmpDf = tmpDf.reset_index(drop=True)
    # tạo mới accum
    tmpDf['Accum Bad Loans'] = 0
    for i in range(0, len(tmpDf)):
        tmpDf.iloc[i, 4] = tmpDf.iloc[i-1, 4] + tmpDf.iloc[i, 2]
    # số row
    n = len(tmpDf)
    print("n : "+str(n))
    d = tmpDf["Bad Loan"].sum()
    print("d : "+str(d))
    step = d/n
    print("step : "+str(step))
    tmpDf['Naive'] = 0
    for i in range(0, len(tmpDf)):
        tmpDf.iloc[i, 5] = tmpDf.iloc[i-1, 5] + step
    print(tmpDf[["Expected Loss", "Bad Loan", "Accum Bad Loans", "Naive"]])
    PerfectModel = 0.5*(1-d/n)
    print("PerfectModel : "+str(PerfectModel))
    ThisModel = (tmpDf["Accum Bad Loans"].sum())/(n*d)-0.5
    AccuracyRatio = ThisModel/PerfectModel
    print("ThisModel : "+str(ThisModel) + "--" "AccuracyRatio : "+str(AccuracyRatio))
    AccuracyRatio = round(AccuracyRatio*100, 0)
    tmpDf = tmpDf.reset_index(drop=True)
    tmpDf['Index'] = tmpDf.index
    # data trả về
    returnData = LoadData(n, d, step,PerfectModel,ThisModel,AccuracyRatio,tmpDf)

    return returnData


dataAll = makeData(df)

dfOldCus = df.loc[df['Customer Type'] == 'Returning']
dfNewCus = df.loc[df['Customer Type'] == 'New']

dataOldCus =  makeData(dfOldCus)
dataNewCus =  makeData(dfNewCus)

print(str(dataAll))

f1 = open('template.html', 'r')
f2 = open('index.html', 'w')
for line in f1:
    new_line = line.replace('###titleAll###', 'Installment Loan Data - Accuracy Ratio = '+str(dataAll.AccuracyRatio)+' %')
    new_line= new_line.replace('###dataAll###', str(dataAll.dfData[['Accum Bad Loans', 'Naive','Index']].to_dict(orient='list')))
    new_line= new_line.replace('###titleOld###', 'Installment Loan Data - Accuracy Ratio = '+str(dataOldCus.AccuracyRatio)+' %')
    new_line= new_line.replace('###dataOld###', str(dataOldCus.dfData[['Accum Bad Loans', 'Naive','Index']].to_dict(orient='list')))
    new_line= new_line.replace('###titleNew###', 'Installment Loan Data - Accuracy Ratio = '+str(dataNewCus.AccuracyRatio)+' %')
    new_line= new_line.replace('###dataNew###', str(dataNewCus.dfData[['Accum Bad Loans', 'Naive','Index']].to_dict(orient='list')))

    f2.write(new_line)
f1.close()
f2.close()
