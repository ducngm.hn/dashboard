import pandas as pd
import os
import os.path as path
import math

pathDataCreated = path.abspath(
    path.join(os.getcwd(), "dataCreated/"))


def SMap(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # All
        # df.columns = ['source', 'target', 'value']
        makeJSONFile(df, "smap_"+time)

    except Exception as e:
        print("#### Error in DB2 SMap", time)


class LoadData(object):
    n = 0
    d = 0
    step = 0
    PerfectModel = 0
    ThisModel = 0
    AccuracyRatio = 0
    dfData = None

    # The class "constructor" - It's actually an initializer
    def __init__(self, n, d, step, PerfectModel, ThisModel, AccuracyRatio, dfData):
        self.n = n
        self.d = d
        self.step = step
        self.PerfectModel = PerfectModel
        self.ThisModel = ThisModel
        self.AccuracyRatio = AccuracyRatio
        self.dfData = dfData


def makeDataGini(tmpDf):
    tmpDf = tmpDf.reset_index(drop=True)
    tmpDf['Index'] = tmpDf.index
    tmpDf = tmpDf.sort_values(
        ["EL", "Index"], ascending=[False, True])
    tmpDf = tmpDf.reset_index(drop=True)
    # tạo mới accum
    tmpDf['Accum Bad Loans'] = 0
    for i in range(0, len(tmpDf)):
        tmpDf.iloc[i, 4] = tmpDf.iloc[i-1, 4] + tmpDf.iloc[i, 2]
    # số row

    n = len(tmpDf)
    # print("n : "+str(n))
    d = tmpDf["Default"].sum()
    # print("d : "+str(d))
    step = d/n
    # print("step : "+str(step))
    tmpDf['Naive'] = 0
    for i in range(0, len(tmpDf)):
        tmpDf.iloc[i, 5] = tmpDf.iloc[i-1, 5] + step
    # print(tmpDf[["Expected Loss", "Bad Loan", "Accum Bad Loans", "Naive"]])
    # print(tmpDf)
    PerfectModel = 0.5*(1-d/n)
    # print("PerfectModel : "+str(PerfectModel))
    ThisModel = (tmpDf["Accum Bad Loans"].sum())/(n*d)-0.5
    AccuracyRatio = ThisModel/PerfectModel
    # print("ThisModel : "+str(ThisModel) +
    #       "--" "AccuracyRatio : "+str(AccuracyRatio))
    AccuracyRatio = round(AccuracyRatio*100, 0)
    tmpDf = tmpDf.reset_index(drop=True)
    tmpDf['Index'] = tmpDf.index + 1
    # data trả về
    returnData = LoadData(n, d, step, PerfectModel,
                          ThisModel, AccuracyRatio, tmpDf)

    return returnData


def Gini(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

        # All
        dataTmp = makeDataGini(df)
        dfTmp = dataTmp.dfData[["Accum Bad Loans", "Naive", "Index"]]
        dfTmp.columns = ['ay', 'ny', 'x']
        makeJSONFileGini(dfTmp, dataTmp.AccuracyRatio, "gini_allcustomers_"+time)

        # Returning
        dfTmp = df[(df["Customer Type"] == "Returning")]
        dataTmp = makeDataGini(dfTmp)
        dfTmp = dataTmp.dfData[["Accum Bad Loans", "Naive", "Index"]]
        dfTmp.columns = ['ay', 'ny', 'x']
        makeJSONFileGini(dfTmp, dataTmp.AccuracyRatio, "gini_existingcustomers_"+time)

        # New
        dfTmp = df[(df["Customer Type"] == "New")]
        dataTmp = makeDataGini(dfTmp)
        dfTmp = dataTmp.dfData[["Accum Bad Loans", "Naive", "Index"]]
        dfTmp.columns = ['ay', 'ny', 'x']
        makeJSONFileGini(dfTmp, dataTmp.AccuracyRatio, "gini_newcustomers_"+time)

    except Exception as e:
        print("#### Error in db2 Gini", time)


def makeJSONFile(tmpDf, name):
    dataName = "db2_"+name
    f1 = open(pathDataCreated+'\\'+dataName+'.js', 'w+')
    f1.write("DATA."+dataName + "= " +
             str(tmpDf.to_json(orient='records')) + ";")
    f1.close()


def makeJSONFileGini(tmpDf, ratio, name):
    dataName = "db2_"+name
    f1 = open(pathDataCreated+'\\'+dataName+'.js', 'w+')
    f1.write(
        "DATA."+dataName+"Title= 'Installment Loan Data - Accuracy Ratio = " + str("{0:.1f}%".format(ratio)) + "'; DATA."+dataName+"= "+str(tmpDf.to_json(orient='records')) + ";")
    f1.close()
