import pandas as pd
import os
import os.path as path
import math

pathDataCreated = path.abspath(
    path.join(os.getcwd(), "dataCreated/"))

def Financial_Summary(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # All
        # df.columns = ['source', 'target', 'value']
        makeJSONFile(df,  "fs_"+time)

    except Exception as e:
        print("#### Error in Financial_Summary", time)

def Loan_Group(df, time):
    try:
        # return "This is the do something method"
        # print(df)
        # All
        dfAll = df[["Title", "All Customers", "Color"]]
        dfAll.columns = ['title', 'value', 'color']
        makeJSONFile(dfAll, "lg_allcustomers_"+time)

        dfAll = df[["Title", "New Customers", "Color"]]
        dfAll.columns = ['title', 'value', 'color']
        makeJSONFile(dfAll, "lg_newcustomers_"+time)

        dfAll = df[["Title", "Existing Customers", "Color"]]
        dfAll.columns = ['title', 'value', 'color']
        makeJSONFile(dfAll, "lg_existingcustomers_"+time)
    except Exception as e:
       print("#### Error in Loan_Group", time)


def CR_Distribution(df, time):
    try:
        # return "This is the do something method"
        # print(df)
        # All
        dfAll = df[["X Axis Label", "All Customers", "All Customers.1"]]
        dfAll.columns = ['x', 'numberofloans', 'defaultodds']
        for i in range(0, len(dfAll)):
            if (math.isnan(dfAll.iloc[i, 2]) or dfAll.iloc[i, 2] <= 0):
                dfAll.iloc[i, 2] = 0
            else:
                break

        makeJSONFile(dfAll, "cr_allcustomers_"+time)

        dfAll = df[["X Axis Label", "New Customers", "New Customers.1"]]
        dfAll.columns = ['x', 'numberofloans', 'defaultodds']
        for i in range(0, len(dfAll)):
            if (math.isnan(dfAll.iloc[i, 2]) or dfAll.iloc[i, 2] <= 0):
                dfAll.iloc[i, 2] = 0
            else:
                break

        makeJSONFile(dfAll, "cr_newcustomers_"+time)

        dfAll = df[["X Axis Label", "Existing Customers", "Existing Customers.1"]]
        dfAll.columns = ['x', 'numberofloans', 'defaultodds']
        for i in range(0, len(dfAll)):
            if (math.isnan(dfAll.iloc[i, 2]) or dfAll.iloc[i, 2] <= 0):
                dfAll.iloc[i, 2] = 0
            else:
                break

        makeJSONFile(dfAll, "cr_existingcustomers_"+time)

    except Exception as e:
        print("#### Error in CR_Distribution", time)


def Default_Pattern(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        for i in range(0, len(df)):
            try:
                df.iloc[i, 0] = "{0:.0f}%".format(df.iloc[i, 0] * 100)
            except Exception:
                pass

        # print(df)
        # All
        dfAll = df[["X Axis Label", "All Customers", "Color"]]
        dfAll.columns = ['x', 'value', 'color']
        makeJSONFile(dfAll, "dp_allcustomers_"+time)

        dfAll = df[["X Axis Label", "New Customers", "Color"]]
        dfAll.columns = ['x', 'value', 'color']
        makeJSONFile(dfAll, "dp_newcustomers_"+time)

        dfAll = df[["X Axis Label", "Existing Customers", "Color"]]
        dfAll.columns = ['x', 'value', 'color']
        makeJSONFile(dfAll, "dp_existingcustomers_"+time)
    except Exception as e:
        print("#### Error in Default_Pattern", time)


def Default_Analysis_Table(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        for i in range(0, len(df)):
            try:
                df.loc[i, "Prob (First Period|Default)"] = "{0:.1f}%".format(
                    df.loc[i, "Prob (First Period|Default)"] * 100)
                df.loc[i, "Average Time to Default / Original Term"] = "{0:.1f}%".format(
                    df.loc[i, "Average Time to Default / Original Term"] * 100)
            except Exception:
                pass
        # All

        makeJSONFile(df, "dpdata_"+time)

    except Exception as e:
        print("#### Error in Default_Analysis_Table", time)


def Rejection_Analysis(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

        # All
        df.columns = ['source', 'target', 'value']
        makeJSONFile(df,  "ra_"+time)

    except Exception as e:
        print("#### Error in Rejection_Analysis", time)


def makeJSONFile(tmpDf, name):
    dataName = "db1_"+name
    f1 = open(pathDataCreated+'\\'+dataName+'.js', 'w+')
    f1.write("DATA."+dataName+"= " +
             str(tmpDf.to_json(orient='records')) + ";")
    f1.close()
