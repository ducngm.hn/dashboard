import pandas as pd
import os
import os.path as path
import math

pathDataCreated = path.abspath(
    path.join(os.getcwd(), "dataCreated/"))


def SMap(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # All
        # df.columns = ['source', 'target', 'value']
        makeJSONFile(df,  "smap_"+time)

    except Exception as e:
        print("#### Error in SMap", time)


def Chart1(df, name, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # print(df)
        # All
        df['Limit'] = 0
        df['NII'] = 0
        df['Loan Loss'] = 0
        makeJSONFile(df, name+"_"+time)

    except Exception as e:
        print("#### Error in db3 Chart1", name, time)


def Chart1Limit(df, time):
    try:
        # return "This is the do something method"
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # print(df)
        # All
        for i in range(0, len(df)):
            if (math.isnan(df.loc[i, "Loan Loss"]) or df.loc[i, "Loan Loss"] <= 0):
                df.loc[i, "Loan Loss"] = 0
            else:
                break
        makeJSONFile(df, "limitshading_"+time)

    except Exception as e:
        print("Error in db3 limitshading", time)


def makeJSONFile(tmpDf, name):
    dataName = "db3_"+name
    f1 = open(pathDataCreated+'\\'+dataName+'.js', 'w+')
    f1.write("DATA."+dataName+"= " +
             str(tmpDf.to_json(orient='records')) + ";")
    f1.close()
