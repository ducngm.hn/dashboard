DATA.portfolio2 = true;
function initDB2(data1, data2) {
	let dataD2 = makeData2_DB2(DATA[data2]);
	makeChart2_DB2(dataD2);

	makeChart1_DB2(DATA[data1], DATA[data1 + "_title"]);
}

function makeChart1_DB2(data, title) {
	chartAMDB2 = AmCharts.makeChart("chartdivAMDB2", {
		"type": "serial",
		"dataProvider": data,
		"titles": [{
			"text": title
		}],
		"legend": {
			"equalWidths": false,
			"valueWidth": 0,
			"position": "bottom",
			"useGraphSettings": true,
			"marginTop": -10
		},
		"valueAxes": [{
			"id": "valueAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"position": "left",
			"minimum": 0,
			"title": "Bad Loans"
		}],
		"balloon": {
			"borderThickness": 1,
			"shadowAlpha": 0
		},
		"graphs": [{
			"id": "g1",
			"bullet": "round",
			"bulletBorderAlpha": 1,
			// "bulletColor": "#ed7d31",
			"fillColors": "#ed7d31",
			"lineColor": "#ed7d31",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "Naïve",
			"useLineColorForBulletBorder": true,
			"balloonFunction": function (graphDataItem, graph) {
				//debugger;
				var value = graphDataItem.values.value.toFixed(2);
				return "<span style='font-size:14px;'>" + value + "</span>";
			},
			//"balloonText": "<span style='font-size:18px;'>[[value]]</span>",
			"valueField": "ny",
			"valueAxis": "valueAxis",
		}, {
			"id": "g2",


			"bullet": "round",
			"bulletBorderAlpha": 1,
			// "bulletColor": "#4472c4",
			"fillColors": "#4472c4",
			"lineColor": "#4472c4",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "Accum Bad Loans",
			"useLineColorForBulletBorder": true,

			"balloonText": "<span style='font-size:14px;'>[[value]]</span>",
			"valueField": "ay",
			"valueAxis": "valueAxis",
		}],
		"chartCursor": {
			"valueLineEnabled": false,
			"valueLineBalloonEnabled": true,
			"cursorAlpha": 0,
			// "zoomable": false,
			// "valueZoomable": true,
			// "valueLineAlpha": 0.5
			"cursorColor": "#000000"
		},

		"categoryField": "x",
		"categoryAxis": {
			"autoGridCount": false,
			// "axisColor": "#555555",
			// "gridAlpha": 0.1,
			"gridColor": "#FFFFFF",
			"gridCount": 20,
			"title": "Customers", "dashLength": 1,
			"minorGridEnabled": true
		}
	});
}

function makeChart2_DB2(data) {
	chartAM2DB2 = AmCharts.makeChart("chartdivAM2DB2", {
		"type": "xy",
		"dataProvider": data,
		// "titles": [{
		// 	"text": "My Chart Title"
		// }],
		"valueAxes": [{
			"position": "bottom",
			"axisAlpha": 0,
			"dashLength": 1,
			"title": "INVESTMENT",
			"minimum": 0
		}, {
			"axisAlpha": 0,
			"dashLength": 1,
			"position": "left",
			"title": "PROFIT TODATE",
			"minimum": 0
		}],
		"startDuration": 0,
		"graphs": [{
			// "balloonText": "x:<b>[[x]]</b> y:<b>[[y]]</b><br>value:<b>[[value]]</b>",
			"balloonFunction": function (graphDataItem, graph) {
				// var value = graphDataItem.values.value.toFixed(2);
				return "<span style='font-size:14px;'>" + graphDataItem.dataContext.title + "</span>";
			},
			"bullet": "circle",
			"bulletBorderAlpha": 0.2,
			"bulletAlpha": 0.8,
			"lineAlpha": 0,
			"fillAlphas": 0,
			"valueField": "v",
			"xField": "x",
			"yField": "y",
			"maxBulletSize": 40,
			"fillColorsField": "lineColor",
			"lineColorField": "lineColor",
		}, {
			"bullet": "none",
			"xField": "Ox",
			"yField": "Oy",
		}],

	});
}

function makeData2_DB2(data) {
	let dataR = [{
		Ox: 0,
		Oy: 0
	}];
	for (let i = 0; i < data.length; i++) {
		let el = data[i];
		let tmpR = {
			x: el["Portfolio Value"],
			y: el["Profit"],
			v: el["Acceptance"],
			title: el["Data Label"]
		};
		// "Data Label": "J",
		// "Portfolio Value": 1423630,
		// "Profit": 90533.5689,
		// "Acceptance": 0.3179
		if (el["Data Label"] == "Current Portfolio") {
			defAccp = el["Acceptance"];
			tmpR.lineColor = "blue";
		} else if (el["Data Label"] == "Adjust Term") {
			tmpR.lineColor = "yellow";
		} else if (el["Data Label"] == "Adjust Term and Amount") {
			tmpR.lineColor = "red";
		} else {
			tmpR.lineColor = "gray";
		}
		dataR.push(tmpR);
	}

	return dataR;
}


function changeChartDB2(data1, data2) {


	if (chartAMDB2 && data1) {


		chartAMDB2.titles[0].text = DATA[data1+"_title"];
		chartAMDB2.validateData();


		chartAMDB2.animateData(DATA[data1], {
			// duration: 1000
		});
	}
	if (chartAM2DB2 && data2) {
		let dataD2 = makeData2_DB2(DATA[data2]);
		chartAM2DB2.animateData(dataD2, {
			duration: 1000
		});
	}
}
