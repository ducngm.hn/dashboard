function changeDataFS(dataName) {
	let comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
	let keys = Object.keys(DATA[dataName][0]);
	for (let i = 0; i < keys.length; i++) {
		let key = keys[i].replace(/[\W_]+/g, '').toLowerCase();
		// if (key === "defaultrate") {
		// 	$("#fs" + key).html(DATA[dataName][keys[i]]);
		// } else {
		// 	$("#fs" + key).animateNumber(
		// 		{
		// 			number: DATA[dataName][keys[i]],
		// 			numberStep: comma_separator_number_step
		// 		}
		// 	);
		// }
		animationChange("#fs" + key, DATA[dataName][keys[i]]);
	}
}

function makePieChartAM(dataName) {
	chartAM = AmCharts.makeChart("chartdivAM", {
		"type": "pie",
		"theme": "light",
		"startDuration": 0.1,
		"dataProvider": DATA[dataName],
		"valueField": "value",
		"titleField": "title",
		"outlineColor": "#FFFFFF",
		"colorField": "color",
		"labelColorField": "color",
		"outlineAlpha": 0.8,
		"outlineThickness": 2
	});
}

function changeChartAM(dataName) {
	chartAM.animateData(DATA[dataName], {
		duration: 1000
	});
}

function makeCRDistributionAM(dataName) {
	chartAM = AmCharts.makeChart("chartdivAM", {
		"type": "serial",
		"startDuration": 1,
		"dataProvider": DATA[dataName],
		"titles": [{
			"text": "Distribution of Credit Ratings"
		}],
		"valueAxes": [{
			"id": "numberofloansAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"position": "left",
			"minimum": 0,
			"title": "Number of Loans"
		}, {
			"id": "defaultoddsAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"position": "right",
			"minimum": 0,
			"title": "Default Odds",
			"labelFunction": function (value) {
				return Math.round(value * 100) + "%";
			},
		}],
		"graphs": [{
			"alphaField": "alpha",
			"balloonText": "[[value]]",
			"fillAlphas": 0.7,
			"legendPeriodValueText": "total: [[value.sum]]",
			"legendValueText": "[[value]]",
			"title": "numberofloans",
			"type": "column",
			"valueField": "numberofloans",
			"valueAxis": "numberofloansAxis",
			"fillColors": "#2962ff",
			"lineColor": "#2962ff"
		}
			,
		{
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"bulletBorderThickness": 1,
			"legendValueText": "[[value]]",
			"title": "defaultodds",
			"fillAlphas": 0,
			"valueField": "defaultodds",
			"valueAxis": "defaultoddsAxis",
			"lineColor": "Orange"
		}],
		"chartCursor": {
			"cursorAlpha": 0.1,
			"cursorColor": "#000000",
			"fullWidth": true,
			"valueBalloonsEnabled": false,
			"zoomable": false
		},
		"categoryField": "x",
		"categoryAxis": {
			"autoGridCount": false,
			"axisColor": "#555555",
			"gridAlpha": 0.1,
			"gridColor": "#FFFFFF",
			"gridCount": 50,
			"title": "CREDIT RATING"
		}
	});
}

function makeDefaultPatternAM(dataName) {
	chartAM = AmCharts.makeChart("chartdivAM", {
		"type": "serial",
		"theme": "light",
		"titles": [{
			"text": "Timing of Default"
		}],

		"marginRight": 70,
		"dataProvider": DATA[dataName],
		"valueAxes": [{
			"axisAlpha": 0,
			"position": "left",
			"minimum": 0,
		}],
		"startDuration": 1,
		"graphs": [{
			"balloonText": "<b>[[category]]: [[value]]</b>",
			"fillColorsField": "color",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "value"
		}],
		"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
		},
		"categoryField": "x",
		"categoryAxis": {
			"gridPosition": "start",
			"labelRotation": 45
		}

	});
}

function changeDPDATA(dataName) {
	for (let i = 0; i < DATA[dataName].length; i++) {
		let ob = DATA[dataName][i];
		let keys = Object.keys(ob);
		let name = ob["DEFAULT ANALYSIS"].replace(/[\W_]+/g, '').toLowerCase();
		for (let j = 0; j < keys.length; j++) {
			try {
				// //newcustomers_default
				// debugger;
				animationChange("#" + name + "_" + keys[j].replace(/[\W_]+/g, '').toLowerCase(), ob[keys[j]]);
			} catch (error) {

			}
		}
	}
}

function animationChange(cssFind, char) {
	let comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
	if ($.isNumeric(char)) {
		$(cssFind).animateNumber(
			{
				number: parseFloat(char),
				numberStep: comma_separator_number_step
			}
		);
	} else {
		$(cssFind).html(char);
	}
}

function sankeyDraw(dataName) {
	try {
		$("#chartdiv3d svg").remove();
	} catch (error) {

	}
	let data = DATA[dataName];

	let units = "Widgets";

	let margin = { top: 10, right: 10, bottom: 10, left: 10 },
		width = 800 - margin.left - margin.right,
		height = 500 - margin.top - margin.bottom;

	let formatNumber = d3.format(",.0f"),    // zero decimal places
		format = function (d) { return formatNumber(d) + " " + units; },
		color = d3.scale.category20();

	// append the svg canvas to the page
	// d3.select("#chartdiv3d").append("<h5>Nice</h5>");
	let svg = d3.select("#chartdiv3d").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform",
			"translate(" + margin.left + "," + margin.top + ")");
	svg.append("text")
		.attr("x", (width / 2))
		.attr("y", 20 - (margin.top / 2))
		.attr("text-anchor", "middle")
		.style("font-size", "18px")
		.style("font-weight", "bold")
		.text("Approved and Declined Applications");
	// Set the sankey diagram properties
	let sankey = d3.sankey()
		.nodeWidth(36)
		.nodePadding(10)
		.size([width, height]);

	let path = sankey.link();

	let graph = {};

	graph.links = data;
	let nodeMap = {};
	graph.nodes = [];
	let dataTmp = graph.links.map(links => ({ name: links.source }));
	let dataTmp2 = graph.links.map(links => ({ name: links.target }));
	dataTmp = dataTmp.concat(dataTmp2);
	let temp = [];
	dataTmp = dataTmp.filter((x, i) => {
		if (temp.indexOf(x.name) < 0) {
			temp.push(x.name);
			return true;
		}
		return false;
	});

	graph.nodes = dataTmp;

	graph.nodes.forEach(function (x) { nodeMap[x.name] = x; });
	graph.links = graph.links.map(function (x) {
		return {
			source: nodeMap[x.source],
			target: nodeMap[x.target],
			value: x.value.toString()
		};
	});

	sankey
		.nodes(graph.nodes)
		.links(graph.links)
		.layout(32);

	// add in the links
	let link = svg.append("g").selectAll(".link")
		.data(graph.links)
		.enter().append("path")
		.attr("class", "link")
		.attr("d", path)
		.style("stroke-width", function (d) { return Math.max(1, d.dy); })
		.sort(function (a, b) { return b.dy - a.dy; });

	// add the link titles
	link.append("title")
		.text(function (d) {
			return d.source.name + " → " +
				d.target.name + "\n" + format(d.value);
		});

	// add in the nodes
	let node = svg.append("g").selectAll(".node")
		.data(graph.nodes)
		.enter().append("g")
		.attr("class", "node")
		.attr("transform", function (d) {
			return "translate(" + d.x + "," + d.y + ")";
		})
		.call(d3.behavior.drag()
			.origin(function (d) { return d; })
			.on("dragstart", function () {
				this.parentNode.appendChild(this);
			})
			.on("drag", dragmove));

	// add the rectangles for the nodes
	node.append("rect")
		.attr("height", function (d) { return d.dy; })
		.attr("width", sankey.nodeWidth())
		.style("fill", function (d) {
			return d.color = color(d.name.replace(/ .*/, ""));
		})
		.style("stroke", function (d) {
			return d3.rgb(d.color).darker(2);
		})
		.append("title")
		.text(function (d) {
			return d.name + "\n" + format(d.value);
		});

	// add in the title for the nodes
	node.append("text")
		.attr("x", -6)
		.attr("y", function (d) { return d.dy / 2; })
		.attr("dy", ".35em")
		.attr("text-anchor", "end")
		.attr("transform", null)
		.text(function (d) { return d.name; })
		.filter(function (d) { return d.x < width / 2; })
		.attr("x", 6 + sankey.nodeWidth())
		.attr("text-anchor", "start");

	// the function for moving the nodes
	function dragmove(d) {
		d3.select(this).attr("transform",
			"translate(" + (
				d.x = Math.max(0, Math.min(width - d.dx, d3.event.x))
			) + "," + (
				d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))
			) + ")");
		sankey.relayout();
		link.attr("d", path);
	}

}