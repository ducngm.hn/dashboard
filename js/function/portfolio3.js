DATA.portfolio3 = true;


const MaxLimit = 2500;
const ELRange = -0.28;

var shadedlimit = 2500;
var curve = 10;
var size = 1;
var defAccp3 = 0;
var defPortfolio3 = 0;
var defProfit3 = 0;

var defaultData_DB3;
var defaultData2_DB3;

var limitShadingData;

$(document).ready(function () {

	//
	// $(".clasloanlimit").show();

	$("#shadedlimit").change(function () {
		// alert($(this).val());
		shadedlimit = parseInt($(this).val());
		whenChange3();
	});
	$("#curve").change(function () {
		curve = parseInt($(this).val());
		whenChange3();
	});
	$("#sizedifferential").change(function () {
		size = parseInt($(this).val());
		changeValueDiff_DB3(size);
	});

});



function initDB3(data1, data2) {
	// let dataD2 = makeData2_DB2(DATA[data2]);
	// makeChart2_DB2(dataD2);

	// makeChart1_DB2(DATA[data1]);
	limitShadingData = DATA[data1];
	defaultData_DB3 = makeData_DB3(DATA[data1]);
	makeChartDB3(defaultData_DB3);

	defaultData2_DB3 = makeData2_DB3(DATA[data2]);
	makeChartDB3_2(defaultData2_DB3);
}





function whenChange3() {
	let newData = [];
	let sumPortfolio = 0;
	let sumNewNII = 0;
	let sumNewLoanLoss = 0;
	let sumProfit = 0;

	//

	for (let i = 0; i < limitShadingData.length; i++) {
		let e = JSON.parse(JSON.stringify(limitShadingData[i]));
		let dataC = calulateForDB3(e.EL, e.Limit, e.Balance, e.NII, e["Loan Loss"]);
		e.Balance = dataC.NewBalance
		newData.push(e);
		sumPortfolio = sumPortfolio + dataC.NewLimit;
		sumNewNII = sumNewNII + dataC.NewNII;
		sumNewLoanLoss = sumNewLoanLoss + dataC.NewLoanLoss;
	}
	sumProfit = sumNewNII - sumNewLoanLoss;
	let makeDataDraw = makeData_DB3(newData);
	changeChart_DB3(makeDataDraw);

	// chart 2 
	for (let i = defaultData2_DB3.length - 1; i > 0; i--) {

		if (defaultData2_DB3[i].lineColor === "green") {

			defaultData2_DB3.splice(i, 1);
			break;
		}
	}

	defaultData2_DB3.push({
		x: sumPortfolio,
		y: sumProfit,
		v: defAccp3,
		lineColor: "green",
		title: "Limit Shading"
	});

	animationChange("#portfolio_withshading", sumPortfolio);
	animationChange("#profit_withshading", sumProfit);

	changeValueDiff_DB3(size);
}

function makeData_DB3(data) {
	let dataR = [];

	let sumPortfolio = 0;
	let sumNII = 0;
	let sumLoanLoss = 0;
	let sumProfit = 0;

	for (let i = 0; i < data.length; i++) {
		let el = data[i];
		if (el["Default"] == 0) {
			dataR.push({
				gLB: el["Balance"],
				gEL: el["EL"]
			});
		} else if (el["Default"] == 1) {
			dataR.push({
				bLB: el["Balance"],
				bEL: el["EL"]
			});
		}
		sumPortfolio = sumPortfolio + el.Limit;
		sumNII = sumNII + el.NII;
		sumLoanLoss = sumLoanLoss + el["Loan Loss"];
	}
	sumProfit = sumNII - sumLoanLoss;
	defPortfolio3 = sumPortfolio;
	defProfit3 = sumProfit;


	animationChange("#portfolio_withoutshading", defPortfolio3);
	animationChange("#profit_withoutshading", defProfit3);
	animationChange("#portfolio_withshading", sumPortfolio);
	animationChange("#profit_withshading", sumProfit);

	return dataR;
}

function makeChartDB3(data) {
	chartAMDB3 = AmCharts.makeChart("chartdivAMDB3", {
		"type": "xy",
		"theme": "light",
		"autoMarginOffset": 0,
		"dataProvider": data,

		"valueAxes": [{
			"position": "bottom",
			"axisAlpha": 0,
			"dashLength": 1,
			"title": "Loan Balance",
			"minimum": 0,

		}, {
			"axisAlpha": 0,
			"dashLength": 1,
			"position": "left",
			"title": "EL",
			"labelFunction": function (value) {
				return Math.round(value * 100) + "%";
			},
			"minimum": 0,

		}],
		"startDuration": 0,
		"graphs": [{
			// "balloonText": "x:[[x]] y:[[y*100]]",
			"bullet": "circle",
			"lineAlpha": 0,
			"xField": "gLB",
			"yField": "gEL",
			"lineColor": "#3366CC",
			"fillAlphas": 0,
			"bulletAlpha": 0.8,
			"bulletSize": 5
		}, {

			"bullet": "circle",
			"lineAlpha": 0,
			"xField": "bLB",
			"yField": "bEL",
			"lineColor": "#DC3912",
			"fillAlphas": 0,
			"bulletAlpha": 0.8,
			"bulletSize": 5
		}],

		// "marginLeft": 64,
		// "marginBottom": 60,
	});
}

function changeChart_DB3(data) {
	if (chartAMDB3) {
		chartAMDB3.animateData(data, {
			duration: 1000
		});
	}
}

function calulateForDB3(EL, Limit, Balance, NII, LoanLoss) {
	let x = (1 + Math.exp(curve * ELRange)) * (MaxLimit - shadedlimit);
	let y = 1 + Math.exp(curve * (EL - 0.288));
	let NewLimit = Math.min(Limit, x / y + shadedlimit);
	let Adjust = NewLimit / Limit;
	let NewBalance = Balance * Adjust;
	let NewNII = NII * Adjust;
	let NewLoanLoss = LoanLoss * Adjust;

	return { NewBalance: NewBalance, NewLimit: NewLimit, NewNII: NewNII, NewLoanLoss: NewLoanLoss };
}

function makeData2_DB3(data) {
	let dataR = [{
		Ox: 0,
		Oy: 0
	}];
	for (let i = 0; i < data.length; i++) {
		let el = data[i];
		let tmpR = {
			x: el["Portfolio Value"],
			y: el["Profit"],
			v: el["Acceptance"],
			title: el["Data Label"],
		};
		// "Data Label": "J",
		// "Portfolio Value": 1423630,
		// "Profit": 90533.5689,
		// "Acceptance": 0.3179
		if (el["Data Label"] == "Current Portfolio") {
			defAccp3 = el["Acceptance"];
			tmpR.lineColor = "blue";
		} else if (el["Data Label"] == "Adjust Term") {
			tmpR.lineColor = "yellow";
		} else if (el["Data Label"] == "Adjust Term and Amount") {
			tmpR.lineColor = "red";
		} else if (el["Data Label"] == "Limit Shading") {
			tmpR.lineColor = "green";
		} else {
			tmpR.lineColor = "gray";
		}
		dataR.push(tmpR);
	}

	if (currentDataDB3 == "limitshading") {

		for (let i = dataR.length - 1; i > 0; i--) {
			if (dataR[i].lineColor === "green") {
				dataR.splice(i, 1);
				break;
			}
		}

		dataR.push({
			x: defPortfolio3,
			y: defProfit3,
			v: defAccp3,
			lineColor: "green",
			title: "Limit Shading"
		});
	}

	return dataR;
}

function makeChartDB3_2(data) {
	chartAM2DB3 = AmCharts.makeChart("chartdivAM2DB3", {
		"type": "xy",
		"dataProvider": data,

		"valueAxes": [{
			"position": "bottom",
			"axisAlpha": 0,
			"dashLength": 1,
			"title": "INVESTMENT",
			"minimum": 0,

		}, {
			"axisAlpha": 0,
			"dashLength": 1,
			"position": "left",
			"title": "PROFIT TODATE",
			"minimum": 0,

		}],
		"startDuration": 0,
		"graphs": [{
			// "balloonText": "x:<b>[[x]]</b> y:<b>[[y]]</b><br>value:<b>[[value]]</b>",
			"balloonFunction": function (graphDataItem, graph) {
				// var value = graphDataItem.values.value.toFixed(2);
				return "<span style='font-size:14px;'>" + graphDataItem.dataContext.title + "</span>";
			},
			"bullet": "circle",
			"bulletBorderAlpha": 0.2,
			"bulletAlpha": 0.8,
			"lineAlpha": 0,
			"fillAlphas": 0,
			"valueField": "v",
			"xField": "x",
			"yField": "y",
			"maxBulletSize": 40,
			"fillColorsField": "lineColor",
			"lineColorField": "lineColor",
		}, {
			"bullet": "none",
			"xField": "Ox",
			"yField": "Oy",
		}],

	});
}

function changeValueDiff_DB3(size) {

	let data = JSON.parse(JSON.stringify(defaultData2_DB3));
	for (let i = 0; i < data.length; i++) {
		if (data[i].v) {
			data[i].v = Math.pow(data[i].v, size);
		}
	}

	if (chartAM2DB3) {
		chartAM2DB3.animateData(data, {
			duration: 1000
		});
	}
}

function changeChartDB3(data1, data2) {
	// var chartAMDB3;
	//     var chartAM2DB3;

	shadedlimit = 2500;
	curve = 10;

	$("#shadedlimit").val(2500);
	$("#curve").val(10);

	if (chartAMDB3 && data1) {
		limitShadingData = DATA[data1];
		let dataD1 = makeData_DB3(DATA[data1]);

		chartAMDB3.animateData(dataD1, {
			duration: 1000
		});
	}

	if (chartAM2DB3 && data2) {
		defaultData2_DB3 = makeData2_DB3(DATA[data2]);
		let dataD2 = JSON.parse(JSON.stringify(defaultData2_DB3));
		// size
		for (let i = 0; i < dataD2.length; i++) {
			if (dataD2[i].v) {
				dataD2[i].v = Math.pow(dataD2[i].v, size);
			}
		}
		chartAM2DB3.animateData(dataD2, {
			duration: 1000
		});
	}
}