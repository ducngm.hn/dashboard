
var chartAM;
var chartAM2;

const MaxLimit = 2500;
const ELRange = -0.28;

var shadedlimit = 2500;
var curve = 10;
var size = 1;
var defAccp = 0;

var defPortfolio = 0;
var defProfit = 0;

var defaultData = makeData(DATA.dataTest);
makeChartDB3(defaultData);

var defaultData2 = makeData2(DATA.dataTest2);
makeChartDB3_2(defaultData2);

$(document).ready(function () {
	$("#radios-time").radiosToSlider();


	$("#shadedlimit").change(function () {
		// alert($(this).val());
		shadedlimit = parseInt($(this).val());
		whenChange();
	});
	$("#curve").change(function () {
		curve = parseInt($(this).val());
		whenChange();
	});
	$("#sizedifferential").change(function () {
		size = parseInt($(this).val());
		changeValueDiff(size);
	});
	//chartdivAM
});

function whenChange() {
	let newData = [];
	let sumPortfolio = 0;
	let sumNewNII = 0;
	let sumNewLoanLoss = 0;
	let sumProfit = 0;

	for (let i = 0; i < DATA.dataTest.length; i++) {
		let e = JSON.parse(JSON.stringify(DATA.dataTest[i]));
		let dataC = calulate(e.EL, e.Limit, e.Balance, e.NII, e["Loan Loss"]);
		e.Balance = dataC.NewBalance
		newData.push(e);
		sumPortfolio = sumPortfolio + dataC.NewLimit;
		sumNewNII = sumNewNII + dataC.NewNII;
		sumNewLoanLoss = sumNewLoanLoss + dataC.NewLoanLoss;
	}
	sumProfit = sumNewNII - sumNewLoanLoss;
	let makeDataDraw = makeData(newData);
	changeChart(makeDataDraw);

	// chart 2 
	for (let i = defaultData2.length - 1; i > 0; i--) {
		
		if (defaultData2[i].lineColor === "red") {
			
			defaultData2.splice(i, 1);
			break;
		}
	}

	defaultData2.push({
		x: sumPortfolio,
		y: sumProfit,
		v: defAccp,
		lineColor: "red"
	});

	animationChange("#portfolio_withshading",sumPortfolio);
	animationChange("#profit_withshading",sumProfit);

	changeValueDiff(size);
}

function makeData(data) {
	let dataR = [];

	let sumPortfolio = 0;
	let sumNII = 0;
	let sumLoanLoss = 0;
	let sumProfit = 0;

	for (let i = 0; i < data.length; i++) {
		let el = data[i];
		if (el["Default"] == 0) {
			dataR.push({
				gLB: el["Balance"],
				gEL: el["EL"]
			});
		} else if (el["Default"] == 1) {
			dataR.push({
				bLB: el["Balance"],
				bEL: el["EL"]
			});
		}
		sumPortfolio = sumPortfolio + el.Limit;
		sumNII = sumNII + el.NII;
		sumLoanLoss = sumLoanLoss + el["Loan Loss"];
	}
	sumProfit = sumNII - sumLoanLoss;
	defPortfolio = sumPortfolio;
	defProfit = sumProfit;


	animationChange("#portfolio_withoutshading",defPortfolio);
	animationChange("#profit_withoutshading",defProfit);
	animationChange("#portfolio_withshading",sumPortfolio);
	animationChange("#profit_withshading",sumProfit);

	return dataR;
}

function makeChartDB3(data) {
	chartAM = AmCharts.makeChart("chartdivAM", {
		"type": "xy",
		"theme": "light",
		"autoMarginOffset": 0,
		"dataProvider": data,
		
		"valueAxes": [{
			"position": "bottom",
			"axisAlpha": 0,
			"dashLength": 1,
			"title": "Loan Balance",
			"minimum": 0,
			"maximum": 2500
		}, {
			"axisAlpha": 0,
			"dashLength": 1,
			"position": "left",
			"title": "EL",
			"labelFunction": function (value) {
				return Math.round(value * 100) + "%";
			},
			"minimum": 0,
			"maximum": 0.9
		}],
		"startDuration": 0,
		"graphs": [{
			// "balloonText": "x:[[x]] y:[[y*100]]",
			"bullet": "circle",
			"lineAlpha": 0,
			"xField": "gLB",
			"yField": "gEL",
			"lineColor": "#3366CC",
			"fillAlphas": 0,
			"bulletAlpha": 0.8,
			"bulletSize": 8
		}, {
			// "balloonText": "x:[[x]] y:[[y*100]]",
			// "balloonFunction": function (graphDataItem, graph) {
			//     var value = graphDataItem.values.value;
			//     if (value < 500) {
			//         return value + "<br>(Little)";
			//     } else {
			//         return value + "<br>(A Lot)";
			//     }
			// },
			"bullet": "circle",
			"lineAlpha": 0,
			"xField": "bLB",
			"yField": "bEL",
			"lineColor": "#DC3912",
			"fillAlphas": 0,
			"bulletAlpha": 0.8,
			"bulletSize": 8
		}],

		// "marginLeft": 64,
		// "marginBottom": 60,
	});
}

function changeChart(data) {
	if (chartAM) {
		chartAM.animateData(data, {
			duration: 1000
		});
	}
}

function calulate(EL, Limit, Balance, NII, LoanLoss) {
	let x = (1 + Math.exp(curve * ELRange)) * (MaxLimit - shadedlimit);
	let y = 1 + Math.exp(curve * (EL - 0.288));
	let NewLimit = Math.min(Limit, x / y + shadedlimit);
	let Adjust = NewLimit / Limit;
	let NewBalance = Balance * Adjust;
	let NewNII = NII * Adjust;
	let NewLoanLoss = LoanLoss * Adjust;

	return { NewBalance: NewBalance, NewLimit: NewLimit, NewNII: NewNII, NewLoanLoss: NewLoanLoss };
}

function makeData2(data) {
	let dataR = [{
		Ox: 0,
		Oy: 0
	}];
	for (let i = 0; i < data.length; i++) {
		let el = data[i];
		let tmpR = {
			x: el["Portfolio Value"],
			y: el["Profit"],
			v: el["Acceptance"],
		};
		// "Data Label": "J",
		// "Portfolio Value": 1423630,
		// "Profit": 90533.5689,
		// "Acceptance": 0.3179
		if (el["Data Label"] == "Current Portfolio") {
			defAccp = el["Acceptance"];
			tmpR.lineColor = "green";
		} else if (el["Data Label"] == "Adjust Term") {
			tmpR.lineColor = "yellow";
		} else if (el["Data Label"] == "Adjust Term and Amount") {
			tmpR.lineColor = "blue";
		} else {
			tmpR.lineColor = "gray";
		}
		dataR.push(tmpR);
	}

	dataR.push({
		x: defPortfolio,
		y: defProfit,
		v: defAccp,
		lineColor: "red"
	});


	return dataR;
}

function makeChartDB3_2(data) {
	chartAM2 = AmCharts.makeChart("chartdivAM2", {
		"type": "xy",
		"dataProvider": data,
		
		"valueAxes": [{
			"position": "bottom",
			"axisAlpha": 0,
			"dashLength": 1,
			"title": "INVESTMENT",
			"minimum": 0,
			"maximum": 2500000
		}, {
			"axisAlpha": 0,
			"dashLength": 1,
			"position": "left",
			"title": "PROFIT TODATE",
			"minimum": 0,
			"maximum": 100000
		}],
		"startDuration": 0,
		"graphs": [{
			// "balloonText": "x:<b>[[x]]</b> y:<b>[[y]]</b><br>value:<b>[[value]]</b>",
			"bullet": "circle",
			"bulletBorderAlpha": 0.2,
			"bulletAlpha": 0.8,
			"lineAlpha": 0,
			"fillAlphas": 0,
			"valueField": "v",
			"xField": "x",
			"yField": "y",
			"maxBulletSize": 40,
			"fillColorsField": "lineColor",
			"lineColorField": "lineColor",
		}, {
			"bullet": "none",
			"xField": "Ox",
			"yField": "Oy",
		}],

	});
}

function changeValueDiff(size) {

	let data = JSON.parse(JSON.stringify(defaultData2));
	for (let i = 0; i < data.length; i++) {
	
		if (data[i].v) {
			data[i].v = Math.pow(data[i].v, size);
		}
	}

	if (chartAM2) {
		chartAM2.animateData(data, {
			duration: 1000
		});
	}
}

function animationChange(cssFind, char) {
	let comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
	if ($.isNumeric(char)) {
		$(cssFind).animateNumber(
			{
				number: parseFloat(char),
				numberStep: comma_separator_number_step
			}
		);
	} else {
		$(cssFind).html(char);
	}
}
