var chart = AmCharts.makeChart("chartdiv", {
	"type": "pie",
	"theme": "light",
	"dataProvider": generateChartData(),
	"legend": {
		"position": "right",
		"marginRight": 100,
		"autoMargins": false
	},
	"valueField": "value",
	"titleField": "category",
	"labelRadiusField": "labelRadius",
	"alphaField": "alpha",
	"startDuration": 0
});


function generateChartData() {
	var chartData = [];

	for (var i = 0; i < 10; i++) {
		var value = Math.floor(Math.random() * 100);
		var labelRadius = Math.floor(Math.random() * 100);
		var alpha = Math.random();

		chartData.push({
			category: "" + i,
			value: value,
			labelRadius: labelRadius,
			alpha: alpha
		});
	}

	return chartData;
}


function loop() {
	var data = generateChartData();

	chart.animateData(data, {
		duration: 1000,
		complete: function () {
			// $('[title="JavaScript charts"]').hide();
			setTimeout(loop, 2000);
			// debugger
		}
	});
}

chart.addListener("init", function () {
	setTimeout(loop, 1000);
	
});