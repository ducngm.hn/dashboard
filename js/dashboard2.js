
var chartAM;
var chartAM2;

const MaxLimit = 2500;
const ELRange = -0.28;

var shadedlimit = 2500;
var curve = 10;
var size = 1;
var defAccp = 0;

var defPortfolio = 0;
var defProfit = 0;

var defaultData2 = makeData2(DATA.dataTest2);
makeChartDB3_2(defaultData2);

$(document).ready(function () {
	$("#radios-time").radiosToSlider();

	makeChartDB3(DATA.dataLine1);

	$(".radios-segment").change(function () {
		let segment = $(this).val().replace(/[\W_]+/g, '').toLowerCase();

		if (segment == "allcustomers") {
			changeChart(DATA.dataLine1);
		} else if (segment == "newcustomers") {
			changeChart(DATA.dataLine2);
		}
		else if (segment == "existingcustomers") {
			changeChart(DATA.dataLine3);
		}

		if (chartAM) {
			chartAM.titles[0].text = segment;
			chartAM.validateData();
		}
	});
});





function makeChartDB3(data) {
	chartAM = AmCharts.makeChart("chartdivAM", {
		"type": "serial",
		"dataProvider": data,
		"titles": [{
			"text": "My Chart Title"
		}],
		"legend": {
			"equalWidths": false,
			"valueWidth": 0,
			"position": "bottom",
			"useGraphSettings": true,
			"marginTop": -10
		},
		"valueAxes": [{
			"id": "valueAxis",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"position": "left",
			"minimum": 0,
			"title": "Bad Loans"
		}],
		"balloon": {
			"borderThickness": 1,
			"shadowAlpha": 0
		},
		"graphs": [{
			"id": "g1",
			"bullet": "round",
			"bulletBorderAlpha": 1,
			// "bulletColor": "#ed7d31",
			"fillColors": "#ed7d31",
			"lineColor": "#ed7d31",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "Naïve",
			"useLineColorForBulletBorder": true,
			"balloonFunction": function (graphDataItem, graph) {
				//debugger;
				var value = graphDataItem.values.value.toFixed(2);
				return "<span style='font-size:14px;'>" + value + "</span>";
			},
			//"balloonText": "<span style='font-size:18px;'>[[value]]</span>",
			"valueField": "ny",
			"valueAxis": "valueAxis",
		}, {
			"id": "g2",


			"bullet": "round",
			"bulletBorderAlpha": 1,
			// "bulletColor": "#4472c4",
			"fillColors": "#4472c4",
			"lineColor": "#4472c4",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "Accum Bad Loans",
			"useLineColorForBulletBorder": true,

			"balloonText": "<span style='font-size:14px;'>[[value]]</span>",
			"valueField": "ay",
			"valueAxis": "valueAxis",
		}],
		"chartCursor": {
			"valueLineEnabled": false,
			"valueLineBalloonEnabled": true,
			"cursorAlpha": 0,
			// "zoomable": false,
			// "valueZoomable": true,
			// "valueLineAlpha": 0.5
			"cursorColor": "#000000"
		},

		"categoryField": "x",
		"categoryAxis": {
			"autoGridCount": false,
			// "axisColor": "#555555",
			// "gridAlpha": 0.1,
			"gridColor": "#FFFFFF",
			"gridCount": 20,
			"title": "Customers", "dashLength": 1,
			"minorGridEnabled": true
		}
	});
}

function changeChart(data) {
	if (chartAM) {
		chartAM.animateData(data, {
			duration: 1000
		});
	}
}

function makeData2(data) {
	let dataR = [{
		Ox: 0,
		Oy: 0
	}];
	for (let i = 0; i < data.length; i++) {
		let el = data[i];
		let tmpR = {
			x: el["Portfolio Value"],
			y: el["Profit"],
			v: el["Acceptance"],
		};
		// "Data Label": "J",
		// "Portfolio Value": 1423630,
		// "Profit": 90533.5689,
		// "Acceptance": 0.3179
		if (el["Data Label"] == "Current Portfolio") {
			defAccp = el["Acceptance"];
			tmpR.lineColor = "green";
		} else if (el["Data Label"] == "Adjust Term") {
			tmpR.lineColor = "yellow";
		} else if (el["Data Label"] == "Adjust Term and Amount") {
			tmpR.lineColor = "blue";
		} else {
			tmpR.lineColor = "gray";
		}
		dataR.push(tmpR);
	}

	return dataR;
}

function makeChartDB3_2(data) {
	chartAM2 = AmCharts.makeChart("chartdivAM2", {
		"type": "xy",
		"dataProvider": data,
		"titles": [{
			"text": "My Chart Title"
		}],
		"valueAxes": [{
			"position": "bottom",
			"axisAlpha": 0,
			"dashLength": 1,
			"title": "INVESTMENT",
			"minimum": 0,
			"maximum": 2500000
		}, {
			"axisAlpha": 0,
			"dashLength": 1,
			"position": "left",
			"title": "PROFIT TODATE",
			"minimum": 0,
			"maximum": 100000
		}],
		"startDuration": 0,
		"graphs": [{
			// "balloonText": "x:<b>[[x]]</b> y:<b>[[y]]</b><br>value:<b>[[value]]</b>",
			"bullet": "circle",
			"bulletBorderAlpha": 0.2,
			"bulletAlpha": 0.8,
			"lineAlpha": 0,
			"fillAlphas": 0,
			"valueField": "v",
			"xField": "x",
			"yField": "y",
			"maxBulletSize": 40,
			"fillColorsField": "lineColor",
			"lineColorField": "lineColor",
		}, {
			"bullet": "none",
			"xField": "Ox",
			"yField": "Oy",
		}],

	});
}

function changeValueDiff(size) {

	let data = JSON.parse(JSON.stringify(defaultData2));
	for (let i = 0; i < data.length; i++) {

		if (data[i].v) {
			data[i].v = Math.pow(data[i].v, size);
		}
	}

	if (chartAM2) {
		chartAM2.animateData(data, {
			duration: 1000
		});
	}
}

function animationChange(cssFind, char) {
	let comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
	if ($.isNumeric(char)) {
		$(cssFind).animateNumber(
			{
				number: parseFloat(char),
				numberStep: comma_separator_number_step
			}
		);
	} else {
		$(cssFind).html(char);
	}
}
