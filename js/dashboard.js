

$(document).ready(function () {
	$("#radios-time").radiosToSlider();
	$('[aria-labelledby="navbarDropdownMenuLink"]').find("a").click(function () {

		$('#navbarDropdownMenuLink').html($(this).html());
	});

	currentChart = "loangroup";
	currentTime = "jul2018";
	currentSegment = "allcustomers";
	currentDB = "db1";
	currentDataDB3 = "currentstrategy";

	changeDB();


	changeDashBoardElement();
	workflowDB();
	// makeDefaultPatternAM();

});

function changeDB() {
	$(".linkdb").click(function () {

		$(".linkdb").attr("class", "nav-item linkdb");
		$(this).attr("class", "nav-item linkdb active");
		let dbName = $(this).children().html();
		if (dbName.indexOf("Portfolio Analysis") == 0) {
			currentDB = "db1";
			$(".classDB1").show();
			$(".classDB2").hide();
			$(".classDB3").hide();
			$(".divSegment").show();
			$(".classDB4").hide();
		} else if (dbName.indexOf("PD Model Performance") == 0) {
			currentDB = "db2";
			$(".classDB1").hide();
			$(".classDB2").show();
			$(".classDB3").hide();
			$(".divSegment").show();
			$(".classDB4").hide();
			loadAndCall("portfolio2", function () {
				let smapData = "db2_" + "smap_" + currentTime;
				let giniData = "db2_" + "gini_" + currentSegment + "_" + currentTime;
				loadAndCall(smapData, function () {
					loadAndCall(giniData, function () {
						initDB2(giniData, smapData);
					});
				});
			}, true);

		} else if (dbName.indexOf("Credit Decision Strategies") == 0) {
			currentDB = "db3";
			$(".classDB1").hide();
			$(".classDB2").hide();
			$(".classDB3").show();
			$(".divSegment").hide();
			$(".classDB4").hide();
			loadAndCall("portfolio3", function () {
				let smapData = "db3_"+"smap_" + currentTime;//currentDataDB3
				let loanData = "db3_" + currentDataDB3 + "_" + currentTime;
				loadAndCall(smapData, function () {
					loadAndCall(loanData, function () {
						initDB3(loanData, smapData);
					});
				});


			}, true);

		} else if (dbName.indexOf("Stress Test Portfolio") == 0) {
			currentDB = "db4";
			$(".classDB1").hide();
			$(".classDB2").hide();
			$(".classDB3").hide();
			$(".divSegment").hide();
			$(".classDB4").show();
		}


	});
}



function changeDashBoardElement() {

	//this.bearer.find('input[type=radio]')
	$('#radios-time input[type=radio]').change(function () {
		let time = $(this).val().replace(/[\W_]+/g, '').toLowerCase();;

		workflowDB(time, null, null);

	});

	$(".radios-segment").change(function () {
		let segment = $(this).val().replace(/[\W_]+/g, '').toLowerCase();

		workflowDB(null, segment, null);

	});
	$('#list-chart a').click(function () {
		let currentC = $(this).html().replace(/[\W_]+/g, '').toLowerCase();

		workflowDB(null, null, currentC);

	});

	$('#list-chartDB3 a').click(function () {
		let dataDB3 = $(this).html().replace(/[\W_]+/g, '').toLowerCase();
		workflowDB(null, null, null, dataDB3);

		if (dataDB3 === "limitshading") {
			$(".clasloanlimit").show();
		} else {
			$(".clasloanlimit").hide();
		}
	});
}

// function workflowDB2(time, segment) {
// 	if (time) {
// 		currentTime = time;
// 		let smapData = "smap_" + currentTime;
// 		let giniData = "gini_" + currentSegment + "_" + currentTime;
// 		loadAndCall(smapData, function () {
// 			//loadAndCall(giniData, function () {
// 			changeChartDB2(giniData, smapData);
// 			//});
// 		});
// 	}
// }
// FS
// LG
// CR
// DP
// DPDATA
// RA
function workflowDB(time, segment, chart, typeLoadDB3) {
	if (time) {
		currentTime = time;
		let dataFile = currentDB + "_fs_" + currentTime;
		loadAndCall(dataFile, function () {
			changeDataFS(dataFile);
		});
		// db1
		if (currentChart == "crdistribution") {
			let myData = currentDB + "_" + "cr" + "_" + currentSegment + "_" + currentTime;
			loadAndCall(myData, function () {
				changeChartAM(myData);
			});
		} else if (currentChart === "loangroup") {
			let myData = currentDB + "_" + "lg" + "_" + currentSegment + "_" + currentTime;
			loadAndCall(myData, function () {
				changeChartAM(myData);
			});

		} else if (currentChart === "defaultpattern") {
			let myData = currentDB + "_" + "dp" + "_" + currentSegment + "_" + currentTime;
			loadAndCall(myData, function () {
				changeChartAM(myData);
			});

		} else if (currentChart === "defaultanalysistable") {
			let myData = currentDB + "_" + "dpdata" + "_" + currentTime;
			loadAndCall(myData, function () {
				changeDPDATA(myData);
			});
		} else if (currentChart === "rejectionanalysis") {
			let myData = currentDB + "_" + "ra" + "_" + currentTime;
			loadAndCall(myData, function () {
				sankeyDraw(myData);
			});
		}
		// db2
		if (chartAMDB2) {
			let smapData = currentDB + "_" + "smap_" + currentTime;
			let giniData = currentDB + "_" + "gini_" + currentSegment + "_" + currentTime;
			loadAndCall(smapData, function () {
				loadAndCall(giniData, function () {
					changeChartDB2(giniData, smapData);
				});
			});
		}

		// db3
		if (chartAMDB3) {
			let smapData = currentDB + "_" + currentTime;
			let loanData = currentDB + "_" + currentDataDB3 + "_" + currentTime;
			loadAndCall(smapData, function () {
				loadAndCall(loanData, function () {
					changeChartDB3(loanData, smapData);
				});
			});
		}
	} else if (segment) {
		currentSegment = segment;
		if (currentChart != "defaultanalysistable" && currentChart != "rejectionanalysis") {
			let myData = "";
			if (currentChart == "crdistribution") {
				myData = currentDB + "_" + "cr" + "_" + currentSegment + "_" + currentTime;
			} else if (currentChart === "loangroup") {
				myData = currentDB + "_" + "lg" + "_" + currentSegment + "_" + currentTime;
			} else if (currentChart === "defaultpattern") {
				myData = currentDB + "_" + "dp" + "_" + currentSegment + "_" + currentTime;
			}
			loadAndCall(myData, function () {
				if (currentChart == "crdistribution") {
					let myData = currentDB + "_" + "cr" + "_" + currentSegment + "_" + currentTime;
					loadAndCall(myData, function () {
						changeChartAM(myData);
					});
				} else if (currentChart === "loangroup") {
					let myData = currentDB + "_" + "lg" + "_" + currentSegment + "_" + currentTime;
					loadAndCall(myData, function () {
						changeChartAM(myData);
					});

				} else if (currentChart === "defaultpattern") {
					let myData = currentDB + "_" + "dp" + "_" + currentSegment + "_" + currentTime;
					loadAndCall(myData, function () {
						changeChartAM(myData);
					});

				}
			});
		}

		// db2
		if (chartAMDB2) {
			let giniData = currentDB + "_" + "gini_" + currentSegment + "_" + currentTime;
			loadAndCall(giniData, function () {
				changeChartDB2(giniData, null);
			});
		}
	} else if (chart) {
		if (chart === "rejectionanalysis") {
			currentChart = chart;
			$("#chartdivAM").hide();
			$("#tableDetail").hide();
			$("#chartprepayment").hide();
			$("#chartcollection").hide();
			$("#chartstoreperformaceanalysis").hide();
			$("#chartdiv3d").show();
			let myData = "db1" + "_" + "ra" + "_" + currentTime;
			loadAndCall(myData, function () {
				sankeyDraw(myData);
			});
		} else if (chart === "defaultanalysistable") {
			currentChart = chart;
			$("#chartdivAM").hide();
			$("#tableDetail").show();
			$("#chartprepayment").hide();
			$("#chartcollection").hide();
			$("#chartstoreperformaceanalysis").hide();
			$("#chartdiv3d").hide();
			let myData = "db1" + "_" + "dpdata" + "_" + currentTime;
			loadAndCall(myData, function () {
				changeDPDATA(myData);
			});
		} else if (chart === "prepayment") {
			$("#chartdivAM").hide();
			$("#tableDetail").hide();
			$("#chartprepayment").show();
			$("#chartcollection").hide();
			$("#chartstoreperformaceanalysis").hide();
			$("#chartdiv3d").hide();
		} else if (chart === "collection") {
			$("#chartdivAM").hide();
			$("#tableDetail").hide();
			$("#chartprepayment").hide();
			$("#chartcollection").show();
			$("#chartstoreperformaceanalysis").hide();
			$("#chartdiv3d").hide();
		} else if (chart === "storeperformaceanalysis") {
			$("#chartdivAM").hide();
			$("#tableDetail").hide();
			$("#chartprepayment").hide();
			$("#chartcollection").hide();
			$("#chartstoreperformaceanalysis").show();
			$("#chartdiv3d").hide();
		} else {
			$("#chartdivAM").show();
			$("#tableDetail").hide();
			$("#chartprepayment").hide();
			$("#chartcollection").hide();
			$("#chartstoreperformaceanalysis").hide();
			$("#chartdiv3d").hide();
			currentChart = chart;
			if (currentChart == "crdistribution") {
				let myData = "db1" + "_" + "cr" + "_" + currentSegment + "_" + currentTime;
				loadAndCall(myData, function () {
					makeCRDistributionAM(myData);
				});
			} else if (currentChart === "loangroup") {
				let myData = "db1" + "_" + "lg" + "_" + currentSegment + "_" + currentTime;
				loadAndCall(myData, function () {
					makePieChartAM(myData);
				});

			} else if (currentChart === "defaultpattern") {
				let myData = "db1" + "_" + "dp" + "_" + currentSegment + "_" + currentTime;
				loadAndCall(myData, function () {
					makeDefaultPatternAM(myData);
				});
			}
		}
	} else if (typeLoadDB3) {
		currentDataDB3 = typeLoadDB3;
		if (chartAMDB3) {
			let smapData = "db3" + "_" + "smap_" + currentTime;
			let loanData = "db3_" + currentDataDB3 + "_" + currentTime;
			loadAndCall(smapData, function () {
				loadAndCall(loanData, function () {
					changeChartDB3(loanData, smapData);
				});
			});
		}
	}
	else {
		let dataFile = "db1_" + "fs_" + currentTime;
		loadAndCall(dataFile, function () {

			changeDataFS(dataFile);
		});
		let myData = "db1" + "_" + "lg" + "_" + currentSegment + "_" + currentTime;
		loadAndCall(myData, function () {

			makePieChartAM(myData);
		});

		$("#chartdivAM").show();
		$("#tableDetail").hide();
		$("#chartdiv3d").hide();
	}

	// chartdivAM
	// tableDetail
	// chartdiv3d


}



function loadJSInternal(fileName, loadFuntion) {
	if (loadFuntion) {
		let file = "js/function/" + fileName + '.js';
		let f = document.createElement('script');
		f.setAttribute('type', 'text/javascript');
		f.setAttribute('src', file);
		document.getElementsByTagName('head')[0].appendChild(f);
	} else {
		var file = "pythonData/dataCreated/" + fileName + '.js';
		var f = document.createElement('script');
		f.setAttribute('type', 'text/javascript');
		f.setAttribute('src', file);
		document.getElementsByTagName('head')[0].appendChild(f);
	}
}

function loadAndCall(fileName, callback, loadFuntion) {
	let checkLoad = false;
	let checkInterval = 200;
	let itvLoad = setInterval(function () {
		if (DATA[fileName] != undefined) {
			clearInterval(itvLoad);
			callback();
		} else {
			if (!checkLoad) {
				loadJSInternal(fileName, loadFuntion);
				checkLoad = true;
			}
			checkInterval--;
			if (checkInterval == 0) {
				console.error("Error load:" + fileName);
				clearInterval(itvLoad);
			}
		}
	}, 50);
}